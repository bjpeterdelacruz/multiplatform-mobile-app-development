# Hybrid Mobile App Development Frameworks

## Setup

1. Install the Ionic framework (`npm install cordova ionic -g`).
2. Install json-server (`npm install json-server -g`).
3. Run json-server (`json-server --watch db.json`).
4. Run app (`ionic serve --lab`).